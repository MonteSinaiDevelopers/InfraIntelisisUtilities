'''
Created on Jun 30, 2017

@author: infra
'''

class BaseHandler(object):
    '''
    classdocs
    '''

    filetypes = ['tbl', 'vis', 'frm', 'dlg', 'rep']
    asm_filetypes = ['etb', 'evi', 'efr', 'edl', 'ere']

    def __init__(self):
        '''
        Constructor
        '''
        