'''
Created on Apr 25, 2017

@author: infra
'''

from pyInfraUtils.LogPrinter import p
from IIULauncher.IIULauncher import c
import IIUHandlers.BaseHandler

import sys
import imaplib
import getpass
import email
import email.header
import datetime
import pymssql

class mail(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''
    conn = False

    def process_mailbox(self, M):
        """
        Do something with emails messages in the folder.  
        For the sake of this example, print some headers.
        """

        rv, data = M.search(None, "ALL")
        if rv != 'OK':
            p.print_info("No messages found!")
            return

        outfile = open("outs/results.txt", "a")
        outfile.write("/********* %s  ********/\n" % (str(datetime.datetime.now())))
        cursor = False
        for num in data[0].split():
            p.print_debug(num)
            rv, data = M.fetch(num, '(RFC822)')
            if rv != 'OK':
                p.print_error("ERROR getting message")
                p.print_debug(num)
                return

            msg = email.message_from_string(data[0][1])
            if('X-Failed-Recipients' in msg):
                decode = email.header.decode_header(msg['X-Failed-Recipients'])[0]
                failed_recipient = unicode(decode[0])
                p.print_debug('Failed: %s' % (failed_recipient))

                cursor = self.conn.cursor(as_dict=True)
                cursor.execute('SELECT * FROM MONSIN_Socios WHERE eMail=%s', failed_recipient)

                for row in cursor:
                    p.print_debug("ID: %d NAME: %s" % ( row['ID_Socio'], row['NombreCompleto']) )
                    outfile.write("UPDATE MONSIN_Socios SET PorActualizarCobranza = 1 WHERE ID_Socio = %s\n" % (row['ID_Socio']))

        p.print_success("Resultados escritos en outs/results.txt")
        
    def main(self):
        M = imaplib.IMAP4_SSL(c.cfg("Mail", "server"))

        try:
            rv, data = M.login(c.cfg("Mail", "account"), c.cfg("Mail", "password"))
        except imaplib.IMAP4.error:
            p.print_error("LOGIN FAILED!!! ")
            sys.exit(1)

        p.print_debug(rv)
        p.print_debug(data)

        self.conn = pymssql.connect(c.cfg("Database", "hostname"), c.cfg("Database", "user"), c.cfg("Database", "password"), c.cfg("Database", "dbname"), autocommit=True)

        rv, mailboxes = M.list()
        #if rv == 'OK':
        #    print "Mailboxes:"
        #    print mailboxes

        rv, data = M.select(c.cfg("Mail", "folder"))
        if rv == 'OK':
            p.print_debug("Processing mailbox...")
            self.process_mailbox(M)
            M.close()
        else:
            p.print_error("ERROR: Unable to open mailbox ")
            p.print_debug(rv)

        M.logout()