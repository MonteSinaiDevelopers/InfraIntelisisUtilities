'''
Created on Apr 25, 2017

@author: infra
'''

from pyInfraUtils.FilesCommon import ScanDirectory
from pyInfraUtils.LogPrinter import p
from IIULauncher.IIULauncher import c, ap
import IIUHandlers.BaseHandler

import os, csv, fileinput, xlsxwriter, pymssql

class status(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''
        
    def getFileStatus(self):
        filestatus = {}
        
        files = ScanDirectory(c.cfg("General", "BaseDir")+"/"+c.cfg("General", "OE-dir"))
        
        if(not files):
            return {'success': 0, 'msg': 'No se encontraron archivos'}
        
        filestatus['files'] = {}
        filestatus['filetypes'] = {} 
        OEdirPath = c.cfg("General", "BaseDir")+"/"+c.cfg("General", "OE-dir")

        for filename, onefile in files.iteritems():
            if(onefile['ext'] != 'esp'):
                continue
            
            if(os.path.isfile(OEdirPath+"/"+filename) == False):
                continue
            
            fileentry = {
                'fullpath': OEdirPath+"/"+filename,
                'name': onefile['name'],
                'ext': onefile['ext']
            }
            
            fileentry['changes'] = self.getChangesFromFile(filename)
            
            filestatus['files'][filename] = fileentry
            
            if(onefile['ext'] not in filestatus['filetypes']):
                filestatus['filetypes'][onefile['ext']] = {}
                
            filestatus['filetypes'][onefile['ext']][filename] = fileentry 
        
        filestatus['success'] = 1
        return filestatus
    
    def getChangesFromFile(self, filename):
        fileChanges = {}
        filepath = c.cfg("General", "BaseDir")+"/"+c.cfg("General", "OE-dir")+"/"+filename        
        if(os.path.isfile(filepath) == False):
            p.print_error("No se encontro el archivo "+filepath)
            return fileChanges
        
        fileext = os.path.splitext(filename)[1][1:]
        if(fileext != 'esp'):
            p.print_error("El archivo "+filepath+" no es un archivo de objeto especial (debe tener extension esp)")
            return fileChanges
        
        lines = [line.rstrip('\n').rstrip('\r').rstrip('\r\n') for line in fileinput.input(filepath, mode='rU')]

        p.print_debug("**** Checando lineas de "+filename+" ****")
        section = 'nosection'
        changefile = 'nofile'

        for oneline in lines:
            p.print_debug("***--- Linea: "+oneline+" ---")
            if(oneline.strip() == ''):
                continue

            if(oneline[:1] == '['):
                p.print_debug("Es seccion, tiene "+str(oneline.count('/'))+" diagonales")
                if(oneline.count('/') == 1):                    
                    splitted = oneline.split('/')
                    section = splitted[1][:-1]
                    changefile = splitted[0][1:]
                    p.print_debug("Seccion: "+section)
                    p.print_debug("ChangeFile: "+changefile)
                    if(changefile not in fileChanges):
                        fileChanges[changefile] = {}
                    if(section not in fileChanges[changefile]):
                        fileChanges[changefile][section] = []
            else:
                p.print_debug("Metiendo cambio en ChangeFile %s Section %s" % (changefile, section))
                if(changefile not in fileChanges):
                    fileChanges[changefile] = {}

                if(section in fileChanges[changefile]):
                    p.print_debug("Cambio de seccion '"+section+"' linea: "+oneline)
                    fileChanges[changefile][section].append(oneline)
                else:
                    fileChanges[changefile][section] = []
                    fileChanges[changefile][section].append(oneline)
                    
        return fileChanges
    
    def getOneFileStatus(self, filename):
        filestatus = {}
        
        filepath = c.cfg("General", "BaseDir")+"/"+c.cfg("General", "OE-dir")+"/"+filename        
        if(os.path.isfile(filepath) == False):
            p.print_error("No se encontro el archivo "+filepath)
            return filestatus
        
        fileext = os.path.splitext(filename)[1][1:]
        if(fileext != 'esp'):
            p.print_error("El archivo "+filepath+" no es un archivo de objeto especial (debe tener extension esp)")
            return filestatus
        
        lines = [line.rstrip('\n') for line in fileinput.input(filepath)]
        lines = lines.pop(0).split('\r')

        p.print_debug("Checando lineas de "+filename)
        fileChanges = self.getChangesFromFile(filename)

        filestatus['files'] = {}
        filestatus['files'][filename] = {}
        fileentry = {
                'fullpath': filepath,
                'name': filename,
                'ext': fileext,
                'changes': fileChanges
            }
        
        filestatus['files'][filename] = fileentry
        
        filestatus['filetypes'] = {}
        filestatus['filetypes'][fileext] = {}    
        filestatus['filetypes'][fileext][filename] = fileentry 
        
        filestatus['success'] = 1
        return filestatus
    
    def writeStatusToCSV(self, filestatus):
        with open('status.csv', 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            
            writer.writerow(["Objeto Especial", "Archivo cambiado", "Seccion", "Cambio"])
            i = 0
            for filename, fileentry in filestatus['files'].iteritems():
                if('changes' not in fileentry or not fileentry['changes']):
                    continue
                
                i+=1
                for onechange in fileentry['changes']:
                    for section,changes in onechange[1].iteritems():
                        for change in changes:
                            writer.writerow([filename, onechange[0], section, change])

        p.print_success("Items totales: "+str(i))

    def writeStatusToXLS(self, filestatus):
        with xlsxwriter.Workbook('outs/status.xlsx', {'constant_memory': True}) as workbook:
            worksheet = workbook.add_worksheet()
            worksheet.set_column(0, 0, 32)
            worksheet.set_column(1, 1, 28)
            worksheet.set_column(2, 2, 40)
            worksheet.set_column(3, 3, 125)
            worksheet.write(0, 0, "Objeto Especial")
            worksheet.write(0, 1, "Archivo cambiado")
            worksheet.write(0, 2, "Seccion")
            worksheet.write(0, 3, "Cambio")
            
            row = 1
            for filename, fileentry in filestatus['files'].iteritems():
                if('changes' not in fileentry or not fileentry['changes']):
                    continue
                
                for changefile,filechanges in fileentry['changes'].iteritems():
                    for section,changes in filechanges.iteritems():
                        for change in changes:
                            p.print_debug("Escribiendo row: "+str(row))
                            p.print_debug("rowdata: {"+filename+", "+changefile+", "+section+", "+change+"}")
                            worksheet.write(row, 0, filename)
                            worksheet.write(row, 1, changefile)
                            worksheet.write(row, 2, section)
                            worksheet.write(row, 3, change)
                            row+=1

        workbook.close()
        p.print_success("Items totales: "+str(row-1))
    
    def printStatusToScreen(self, filestatus):
        i = 0
        for filename, fileentry in filestatus['files'].iteritems():
            if('changes' not in fileentry or not fileentry['changes']):
                continue

            for changefile,filechanges in fileentry['changes'].iteritems():
                for section,changes in filechanges.iteritems():
                    for change in changes:
                        i+=1
                        p.print_info("File: '"+filename+"'. ChangeFile: '"+changefile+"'. Section: '"+section+"'. Line: '"+change+"'")

        p.print_success("Items totales: "+str(i))
        
    def saveResultsToDB(self, filestatus):
        with pymssql.connect(c.cfg("Database", "hostname"), c.cfg("Database", "user"), c.cfg("Database", "password"), c.cfg("Database", "dbname"), autocommit=True, charset="ISO-8859-1") as conn:
            with conn.cursor(as_dict=True) as cursor:
                tablename = 'status_'+c.cfg("General", "OE-dir")
                p.print_debug("TableName: "+tablename)
                cursor.execute("""
                    SELECT COUNT(*) as "count"
                    FROM information_schema.tables
                    WHERE table_name = '{0}'
                    """.format(tablename.replace('\'', '\'\'')))
                if cursor.fetchone()["count"] == 0:
                    #create table
                    cursor.execute("""
                        CREATE TABLE {0} (
                            EspFilename VARCHAR(255) NOT NULL,
                            ChangeFilename VARCHAR(255) NOT NULL,
                            Section VARCHAR(255) NOT NULL,
                            Change VARCHAR(max) NOT NULL,
                        )
                        """.format(tablename.replace('\'', '\'\'')))
                
                cursor.execute('TRUNCATE TABLE {0}'.format(tablename))
                for filename, fileentry in filestatus['files'].iteritems():
                    if('changes' not in fileentry or not fileentry['changes']):
                        continue
                    
                    for changefile,filechanges in fileentry['changes'].iteritems():
                        for section,changes in filechanges.iteritems():
                            for change in changes:
                                query = unicode('INSERT INTO {} (EspFilename, ChangeFilename, Section, Change) VALUES (\'{}\', \'{}\', \'{}\', \'{}\')'.format(tablename, filename, changefile, section, change.replace('\'', '\'\'')))
                                p.print_debug(query)
                                cursor.execute(query)
                
        return
        
    def main(self):
        if(ap.args['action'] == 'status-one'):
            if('file' not in self.arg_list):
                p.print_error("Se definio status-one como modo pero no que archivo revisar")
            else:
                filestatus = self.getOneFileStatus(self.arg_list['file'])
        else:
            filestatus = self.getFileStatus()
        
        if(not filestatus or 'success' not in filestatus):
            p.print_info("No hay estatus")
            return
            
        if(filestatus['success'] == 0):
            p.print_error(filestatus['msg'])
            return
        
        if(not filestatus['files']):
            p.print_info("No se encontraron archivos")
            return
        
        if('display' not in ap.args or ap.args['display'] == None):
            outputType = 'screen'
        else:
            outputType = ap.args['display']
        p.print_debug("Output a %s" % (outputType))

        if(outputType == 'csv'):
            self.writeStatusToCSV(filestatus)
        elif(outputType == 'xls'):
            self.writeStatusToXLS(filestatus)
        elif(outputType == 'screen'):
            self.printStatusToScreen(filestatus)
        elif(outputType == 'database' or outputType == 'db'):
            self.saveResultsToDB(filestatus)