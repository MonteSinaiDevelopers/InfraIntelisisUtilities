# -*- coding: utf-8 -*-
'''
Created on Jan 1, 2018

@author: infra
'''

# para que esto sirva, es necesario bajar el CLI de AWS con
# pip install awscli
# y configurar las credenciales

from pyInfraUtils.LogPrinter import p
from IIULauncher.IIULauncher import ap
import IIUHandlers.BaseHandler
from IIULauncher.IIULauncher import c

import os, re, boto3
from datetime import datetime
from botocore.exceptions import ClientError

import binascii
import concurrent.futures
import hashlib
import math
import os.path
import sys
import tarfile
import tempfile
import threading

import pyInfraUtils.GlacierUploader as gu
import pyInfraUtils.GlacierJob as gj

class upload(IIUHandlers.BaseHandler.BaseHandler):
    config = None

    '''
    classdocs
    '''

    def upload_files(self):
        vaultName = 'RespaldosIntelisis'
        filename = {0: '\\\\Mspdc\\respaldos\\2020.02.11_2200horas.bak'}

        uploader = gu.GlacierUploader()
        basename = os.path.basename(filename[0])
        archive = uploader.upload(vaultName, filename, None)
        if archive is not None:
            p.print_info('Archive %s added to %s' % (archive["archiveId"], vaultName))

    def get_vault_inventory(self, vaultName):
        response = None

        glacier_job = gj.GlacierJob()

        response_job = glacier_job.initiate_job(vaultName, {'Type': 'inventory-retrieval'})
        while response == None:
            p.print_debug(f'Esperando respuesta de jobid {response_job["jobId"]}')
            response = glacier_job.retrieve_inventory_results(vaultName, response_job["jobId"])

        print_info(inventory['ArchiveList'])

    def main(self):
        vaultFiles = self.get_vault_inventory('RespaldosIntelisis')
        self.upload_files()
        return