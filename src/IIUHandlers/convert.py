# -*- coding: utf-8 -*-
'''
Created on Apr 25, 2017

@author: infra
'''

from pyInfraUtils.FilesCommon import ScanDirectory
import os, re, ntpath, shutil, fileinput, io

from IIULauncher.IIULauncher import c
from __builtin__ import file
import IIUHandlers.BaseHandler
from pyInfraUtils.LogPrinter import p

class convert(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''
    filetypes = ['tbl', 'vis', 'frm', 'dlg', 'rep']
    asm_filetypes = ['etb', 'evi', 'efr', 'edl', 'ere']
    
    accent_letters = [(unichr(193), 'Á'), (unichr(201), 'É'), (unichr(205), 'Í'), (unichr(211), 'Ó'), (unichr(218), 'Ú'), (unichr(225), 'á'), (unichr(233), 'é'), (unichr(237), 'í'), (unichr(243), 'ó'), (unichr(250), 'ú')]
        
    def convertEspToAsm(self, filepath):
        p.print_debug("Convirtiendo a ASM: "+filepath)
        if(os.path.isfile(filepath) == False):
            p.print_error("No existe: "+filepath)
            return False
        
        lines = [line.rstrip('\n').rstrip('\r').rstrip('\r\n') for line in fileinput.input(filepath, mode='rU')]
        
        section = ''
        asmFileName = ''
        for oneline in lines:
            if(oneline.strip() == ''):
                continue
            
            p.print_debug("Linea: "+oneline)
            if(oneline[:1] == '['):
                if(oneline.count('/') == 1):                    
                    splitted = oneline.split('/')
                    asmFileName = splitted[0][1:]
                    replacements = {".tbl": ".etb", ".vis": ".evi", ".frm": ".efr", ".dlg": ".edl"}
                    replacements = dict((re.escape(k), v) for k, v in replacements.iteritems())
                    pattern = re.compile("|".join(replacements.keys()))
                    asmFileName = pattern.sub(lambda m: replacements[re.escape(m.group(0))], asmFileName)
                    
                    section = splitted[1][:-1]                    
                        
                    oneline = ('\n' + '['+section+']')
                    
                    asmFileName = c.get("General", "BaseDir")+'/'+c.get("General", "project-dir")+'/'+asmFileName
                    
            if(asmFileName == ''):
                p.print_error("El nombre archivo ASM objetivo esta vacio. OE: "+filepath)
                continue
            
            p.print_debug("Intentando abrir archivo: '%s'" % (asmFileName))
            try:
                asmFile = io.open(asmFileName, 'a+', newline='\r\n')
                asmFile.write(u''+oneline + '\n')
            except IOError:
                p.print_error("Error: no existe el archivo '"+asmFileName+"'")
                continue
            
    def convertAsmToEsp(self, filepath):
        p.print_debug("Convirtiendo a ESP: "+filepath)
        if(os.path.isfile(filepath) == False):
            p.print_error("No existe: "+filepath)
            return False
        
        lines = [line.rstrip('\n').rstrip('\r').rstrip('\r\n') for line in fileinput.input(filepath, mode='rU')]
        
        espFileName = ntpath.basename(filepath)
        espBaseFileName = os.path.splitext(espFileName)[0]
        ext = os.path.splitext(filepath)[1][1:]
        replacements = {"etb": "tbl", "evi": "vis", "efr": "frm", "edl": "dlg"}
        replacements = dict((re.escape(k), v) for k, v in replacements.iteritems())
        pattern = re.compile("|".join(replacements.keys()))
        ext = pattern.sub(lambda m: replacements[re.escape(m.group(0))], ext)
        espFileName = espBaseFileName+'-'+ext+'.esp'
        
        try:
            espFile = io.open(c.get("General", "BaseDir")+'/'+c.get("General", "OE-dir")+'/'+espFileName, 'a+', newline='\r\n')
        except IOError:
            print "Error: no existe el archivo '"+espFileName+"'"
            return
        
        for oneline in lines:
            p.print_debug("Linea: "+oneline)
            if(oneline.strip() == ''):
                continue
            
            if(oneline.startswith('[')):
                oneline = oneline.replace('[', '['+espBaseFileName+'.'+ext+'/') 
                
            for k, v in self.accent_letters:
                oneline = u''+oneline.replace(v, k)

            espFile.write(oneline + '\n')
            
        p.print_debug("Convertido: "+espFileName)
            
    def copyFileToTarget(self, filepath):
        if(os.path.isfile(filepath) == False):
            return False
        
        shutil.copy2(filepath, c.get("General", "BaseDir")+'/'+c.get("General", "project-dir")+'/')
        
    def copyFileToDOE(self, filepath):
        p.print_debug("Copiando a DOE: "+filepath)
        if(os.path.isfile(filepath) == False):
            return False
        
        shutil.copy2(filepath, c.get("General", "BaseDir")+'/'+c.get("General", "OE-dir")+'/')
        
    def convertOE(self):
        scandir = c.get("General", "BaseDir")+'/'+c.get("General", "OE-dir")
        files = ScanDirectory(scandir)
        
        for onefile in files.iteritems():
            if(onefile[1]['ext'] == 'esp'):
                self.convertEspToAsm(onefile[1]['fullpath'])
            elif onefile[1]['ext'] in self.filetypes:
                self.copyFileToTarget(onefile[1]['fullpath'])
                
    def convertAddonToOE(self, filepath):
        if(os.path.isfile(filepath) == False):
            return False
        
        lines = [line.rstrip('\n') for line in open(filepath)]
        
        targetfile = None
        targetfilename = ""
        for oneline in lines:
            if(oneline[:-1] != "<![CDATA[" and oneline.startswith("<") and str(oneline[:-1].endswith(">"))):
                targetfilename = c.get("General", "BaseDir")+'/'+c.get("General", "project-dir")+"/"+oneline[1:][:-2]
                if(targetfilename[-3:] in self.filetypes):
                    #print_debug("Creando archivo "+targetfilename)
                    targetfile = open(targetfilename, 'a+')
            else:
                if(type(targetfile) == file and oneline[:-1] != "<![CDATA[" and oneline[:-1] != "]]>"):
                    targetfile.write(oneline)
                
    def convertAddons(self):
        scandir = c.get("General", "BaseDir")+'/'+"AddOns"
        files = ScanDirectory(scandir)
        
        for onefile in files.iteritems():
            if(onefile[1]['ext'].lower() == 'xml'):
                self.convertAddonToOE(onefile[1]['fullpath'])
                
    def convertASM(self):
        scandir = c.get("General", "BaseDir")+'/'+c.get("General", "project-dir")
        files = ScanDirectory(scandir)
        
        for onefile in files.iteritems():
            if(onefile[1]['ext'] in self.asm_filetypes):
                self.convertAsmToEsp(onefile[1]['fullpath'])
            elif onefile[1]['ext'] in self.filetypes:
                self.copyFileToDOE(onefile[1]['fullpath'])
        
    def main(self):
        if('origin' not in self.arg_list):
            p.print_error("No se definio que convertir")
            return
            
        if(self.arg_list['origin'] == 'OE'):
            self.convertOE()
        elif(self.arg_list['origin'] == 'OE-single'):
            if('file' in self.arg_list and self.arg_list['file'].strip() != ''):
                self.convertEspToAsm(c.cfg("General", "BaseDir")+'/'+c.cfg("General", "OE-dir")+'/'+self.arg_list['file'].strip())
            else:
                p.print_error("Se eligio convertir un ESP especifico pero no se definio el nombre del objeto")
        elif(self.arg_list['origin'] == 'addons'):
            self.convertAddons()
        elif(self.arg_list['origin'] == 'ASM'):
            self.convertASM()
        elif(self.arg_list['origin'] == 'ASM-single'):
            if(self.arg_list['file'].strip() != ''):
                self.convertAsmToEsp(c.get("General", "BaseDir")+'/'+c.get("General", "project-dir")+'/'+self.arg_list['file'].strip())
            else:
                p.print_error("Se eligio convertir un ASM especifico pero no se definio el nombre del objeto")