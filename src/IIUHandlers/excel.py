# -*- coding: utf-8 -*-
'''
Created on Jan 1, 2018

@author: infra
'''

from pyInfraUtils.LogPrinter import p
import IIUHandlers.BaseHandler

import os, io, datetime, xlrd, ntpath, unicodedata
from types import NoneType

from pyInfraUtils.Config import pIUConfig
from IIULauncher.IIULauncher import c, ap

class excel(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''
    accent_letters = [(unichr(193), 'A'), (unichr(201), 'E'), (unichr(205), 'I'), (unichr(211), 'O'), (unichr(218), 'U'), (unichr(225), 'a'), (unichr(233), 'e'), (unichr(237), 'i'), (unichr(243), 'o'), (unichr(250), 'u')]
    workbook = None
    sheet = None
    fields = {}
    config_excel = None

    def printColumnNames(self, filepath):
        filename = os.getcwd()+'/'+filepath
        p.print_info("Leyendo: %s" % (filepath))
        if(os.path.isfile(filepath) == False):
            p.print_error("No se encontro el archivo "+filename)
            p.print_debug(filepath)
            return
           
        self.workbook = xlrd.open_workbook(filepath)
        sheetname = self.config_excel.cfg("Input", "sheetname")

        p.print_debug(self.workbook.sheet_names())
        if sheetname in self.workbook.sheet_names():
            p.print_info("Usando Sheet "+sheetname)
            self.sheet = self.workbook.sheet_by_name(sheetname)
        else:
            p.print_info("No existe la sheet %s. Usando la 0" % (sheetname))
            self.sheet = self.workbook.sheet_by_index(0)


        number_of_cols = self.sheet.ncols

        for colno in range(0, number_of_cols):
            fieldname = self.sheet.cell_value(0, colno)
            p.print_info(str(colno)+") "+str(fieldname))
            self.fields[colno] = {
                'fieldname': self.sheet.cell_value(0, colno) 
            }
                
        
        return
    
    def writeToFileObject(self, afile, line):
        if type(line) == str:
            for k, v, in self.accent_letters:
                line = line.replace(k, v)
        elif type(line) == unicode:
            for k, v, in self.accent_letters:
                line = line.replace(k, v)
                line = unicodedata.normalize('NFKD', line).encode('ascii','ignore')

        afile.write(u'' + line + '\n')

    def process(self):
        numer_of_rows = self.sheet.nrows

        template = self.config_excel.cfg('Query', 'Template')

        if 'file' not in self.arg_list or type(self.arg_list['file']) == NoneType or self.arg_list['file'].strip() == '':
            filename = 'output-'+os.path.splitext(os.path.basename(self.config_excel.cfg('Input', 'file')).strip())[0]+'.sql'
        else:
            filename = ntpath.basename(self.arg_list['file'])
        p.print_debug("outfile: "+filename)
        
        sqlFile = io.open('outs/'+filename, 'w+', newline='\r\n')
        sqlFile.close()
        sqlFile = io.open('outs/'+filename, 'a+', newline='\r\n')
        
        self.writeToFileObject(sqlFile, 'BEGIN TRANSACTION')
        self.writeToFileObject(sqlFile, self.config_excel.cfg("Query", "before"))

        for row in range(1, numer_of_rows):
            value_list = {}
            row_template = template
            
            for colno, field  in self.fields.iteritems():
                val = self.sheet.cell_value(row, colno)
                if(self.sheet.cell_type(row, colno) == 3):
                    val = datetime.datetime(*xlrd.xldate_as_tuple(val, self.workbook.datemode))
                
                if isinstance(val, basestring) == False:
                    # Hack para que los telefonos no les ponga un ".0" ya que Excel reporta todos los numeros como float.
                    # ToDo: Que tal si en la celda hay un float que se tiene que quedar como float?
                    if(isinstance(val, float)):
                        val = int(val)
                    val = str(val)
                else:
                    val = val.replace("'","''")

                value_list[colno] = val

            for i in range(0, len(self.fields)):
                row_template = row_template.replace('#f'+str(i)+'#', self.fields[i]['fieldname'])

            for i in range(0, len(value_list)):
                row_template = row_template.replace('#v'+str(i)+'#', str(value_list[i].encode('utf8')))

            self.writeToFileObject(sqlFile, row_template)
        
        self.writeToFileObject(sqlFile, self.config_excel.cfg("Query", "after"))    
        self.writeToFileObject(sqlFile, 'ROLLBACK')

    def main(self):
        self.arg_list = ap.args
        filepath = self.arg_list['origin'].strip()

        p.print_debug('Config: '+os.getcwd()+'/'+filepath)
        self.config_excel = pIUConfig(os.getcwd()+'/'+filepath)

        inputfile = self.config_excel.cfg('Input', 'file')
        p.print_debug("XLS file:" + inputfile)

        self.printColumnNames(inputfile)
        
        self.process()
        
        