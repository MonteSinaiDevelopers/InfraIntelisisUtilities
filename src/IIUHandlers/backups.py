# -*- coding: utf-8 -*-
'''
Created on Jan 1, 2018

@author: infra
'''

# para que esto sirva, es necesario bajar el CLI de AWS con
# pip install awscli
# y configurar las credenciales

from pyInfraUtils.LogPrinter import p
from IIULauncher.IIULauncher import ap
import IIUHandlers.BaseHandler
from IIULauncher.IIULauncher import c

import os, re, boto3
from datetime import datetime
from botocore.exceptions import ClientError

import binascii
import concurrent.futures
import hashlib
import math
import os.path
import sys
import tarfile
import tempfile
import threading

import pyInfraUtils.GlacierUploader as gu
import pyInfraUtils.GlacierJob as gj

class backups(IIUHandlers.BaseHandler.BaseHandler):
    config = None

    '''
    classdocs
    '''

    def main(self):
        paths = c.cfg("Backups", "check_paths").split(",")
        p.print_debug(paths)

        pattern = re.compile('^[0-9][0-9][0-9][0-9]\.[0-9][0-9]\.[0-9][0-9]*')
        now = datetime.now()

        for path in paths:
            p.print_debug('Entrando a '+path)
            path = bytes(path, encoding='utf8').decode("unicode_escape")

            if(os.path.isdir(path+'\\del') == False):
                p.print_error('El directorio %s no existe' % (path+'\\del'))

            try:
                pathlist = os.listdir(path)
            except:
                p.print_error("Ocurrio un error al ver el contenido de %s" % (path))
                continue

            for filename in pathlist:
                matches = pattern.search(filename)
                if matches == None:
                    p.print_debug('Salto %s por no ser formato' % (filename))
                    continue

                p.print_debug('Checando '+filename)

                year = filename[:4]
                month = filename[5:7]
                day = filename[8:10]
                
                filedate = datetime.strptime('%s-%s-%s' % (year, month, day), '%Y-%m-%d')
                diff = now - filedate
                if(diff.days > 60):
                    if(ap.arg("dryrun") == False):
                        p.print_info('Moviendo %s a %s por >60 dias' % (path+'\\'+filename, path+'\\del\\'+filename))
                        os.remove(path+'\\'+filename)
                    else:
                        p.print_info('[DRY] Moviendo %s a %s por >60 dias' % (path+'\\'+filename, path+'\\del\\'+filename))
                    continue

                if(diff.days > 8 and filedate.weekday() != 6):
                    if(ap.arg("dryrun") == False):
                        p.print_info('Moviendo %s a %s por >8 dias y no es domingo' % (path+'\\'+filename, path+'\\del\\'+filename))
                        os.remove(path+'\\'+filename)
                    else:
                        p.print_info('[DRY] Moviendo %s a %s por >8 dias y no es domingo' % (path+'\\'+filename, path+'\\del\\'+filename))
                    continue

                p.print_info('Se queda %s porque es Domingo y <60' % (filename))

        return