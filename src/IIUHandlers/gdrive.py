# -*- coding: utf-8 -*-
'''
Created on Jan 1, 2018

@author: infra
'''

#imports para conexion a APIs de Google
from __future__ import print_function
import pickle
import io
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient.http import MediaIoBaseDownload

from pyInfraUtils.LogPrinter import p
import IIUHandlers.BaseHandler

SCOPES = ['https://www.googleapis.com/auth/drive.readonly']

class gdrive(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''
    resource = None

    def connect(self):
        """Shows basic usage of the Drive v3 API.
        Prints the names and ids of the first 10 files the user has access to.
        """
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.resource = build('drive', 'v3', credentials=creds)

    def listFiles(self):
        # Call the Drive v3 API
        results = self.resource.files().list(
            pageSize=10, fields="nextPageToken, files(id, name, mimeType, parents)", q="'root' in parents").execute()
        items = results.get('files', [])

        if not items:
            p.print_info('No files found.')
        else:
            p.print_success(items)

    def downloadFile(self, fileid, output_filename):
        request = self.resource.files().export_media(fileId=fileid, mimeType='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        fh = open(output_filename, "wb+")

        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            p.print_info(status.progress() * 100)

        p.print_debug(downloader)

    def getFileId(self, filepath):
        path_parts = filepath.split('/')

        parent = 'root'
        for dir in path_parts:
            foundfile = None
            p.print_debug("Buscando archivo %s en parent %s" % (dir, parent))
            parent_object = self.resource.files().get(fileId=parent).execute()
            p.print_debug({'1': 'parent', '2': parent_object})

            filename = os.path.splitext(dir)[0]

            results = self.resource.files().list(fields="files(id, name, mimeType, parents)", q="'%s' in parents and name = '%s'" % (parent, filename)).execute()
            p.print_debug({'1encontrado': 'results[files]', '2data': results[u'files']})

            if len(results[u'files']) == 0:
                p.print_error("No se encontro el objeto %s en el folder %s" % (dir, parent_object['name']))
                results_folderlist = self.resource.files().list(fields="files(id, name, mimeType, parents)", q="'%s' in parents" % (parent)).execute()
                p.print_debug({'1folderfiles': 'archivos en %s' % (parent_object['name']), '2data': results_folderlist})
            
            elif len(results[u'files']) > 1:
                p.print_error("se encontraron mas de 1 objecto con nombre %s en el folder %s" % (dir, parent_object['name']))   
                for file in results[u'files']:
                    p.print_debug(file)
            else:
                foundfile = results[u'files'][0]
                parent = foundfile['id']

        p.print_debug({'1var': 'foundfile', 'data': foundfile})
        return foundfile

    def main(self):
        self.connect()

        #self.listFiles()

        fileid = self.getFileId('Casos Comité de Honor y Justicia (NEGOCIOS)')

        self.downloadFile(fileid[u'id'], 'ins/Casos2019-gdrive.xlsx')