'''
Created on Jan 1, 2018

@author: infra
'''

from pyInfraUtils.LogPrinter import p
import IIUHandlers.BaseHandler

import os, win32api

class lic(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''

    def SL_MD5Init(var Context: SL_MD5Context);
    begin
      with Context do begin
        SL_State[0] := $67452301;
        SL_State[1] := $efcdab89;
        SL_State[2] := $98badcfe;
        SL_State[3] := $10325476;
        SL_Count[0] := 0;
        SL_Count[1] := 0;
        ZeroMemory(@SL_Buffer, SizeOf(SL_MD5Buffer));
      end;
    end;

    def SL_MD5Update(var Context: SL_MD5Context; Input: pChar; Length: longword);
    var
      Index: longword;
      PartLen: longword;
      I: longword;
    begin
      with Context do begin
        Index := (SL_Count[0] shr 3) and $3f;
        inc(SL_Count[0], Length shl 3);
        if SL_Count[0] < (Length shl 3) then inc(SL_Count[1]);
          inc(SL_Count[1], Length shr 29);
      end;
      PartLen := 64 - Index;
      if Length >= PartLen then begin
        CopyMemory(@Context.SL_Buffer[Index], Input, PartLen);
        SL_Transform(@Context.SL_Buffer, Context.SL_State);
        I := PartLen;
        while I + 63 < Length do begin
          SL_Transform(@Input[I], Context.SL_State);
          inc(I, 64);
        end;
        Index := 0;
      end else
        I := 0;
      CopyMemory(@Context.SL_Buffer[Index], @Input[I], Length - I);
    end;

    def SL_MD5Final(var Context: SL_MD5Context; var Digest: SL_MD5Digest);
    var
      Bits: SL_MD5CBits;
      Index: longword;
      PadLen: longword;
    begin
      SL_Decode(@Context.SL_Count, @Bits, 2);
      Index := (Context.SL_Count[0] shr 3) and $3f;
      if Index < 56 then PadLen := 56 - Index else PadLen := 120 - Index;
      SL_MD5Update(Context, @PADDING, PadLen);
      SL_MD5Update(Context, @Bits, 8);
      SL_Decode(@Context.SL_State, @Digest, 4);
      ZeroMemory(@Context, SizeOf(SL_MD5Context));
    end;

    def SL_MD5String(M: string): SL_MD5Digest;
    var
      Context: SL_MD5Context;
    begin
      SL_MD5Init(Context);
      SL_MD5Update(Context, pChar(M), length(M));
      SL_MD5Final(Context, Result);
    end;

    def SL_MD5Print(D: SL_MD5Digest): string;
    var
      I: byte;
    const
      Digits: Array[0..15] of char =
              ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
    begin
      Result := '';
      for I := 0 to 15 do Result := Result + Digits[(D[I] shr 4) and $0f] + Digits[D[I] and $0f];
    end;

    def SL_Firmar(rp, rl, sk): String;
        rp = rp.upper()
        rl = rl.upper()
        sk = sk.upper()
        dd = "CAE6-6E5E"

        v1 = "INTELISISSL"
        v2 = "CONTROL"

        drp = SL_MD5String(rp);
        drl = SL_MD5String(rl);
        dsk = SL_MD5String(sk);
        ddd = SL_MD5String(dd);
        dv1 = SL_MD5String(v1);
        dv2 = SL_MD5String(v2);

        rp:=UpperCase(SL_MD5Print(drp));
        rl:=UpperCase(SL_MD5Print(drl));
        sk:=UpperCase(SL_MD5Print(dsk));
        dd:=UpperCase(SL_MD5Print(ddd));
        v1:=UpperCase(SL_MD5Print(dv1));
        v2:=UpperCase(SL_MD5Print(dv2));
        ff:=rp+v1+sk+dd+v2+rl;
        dff:=SL_MD5String(ff);
        s:=UpperCase(SL_MD5Print(dff));

        Result:=s;

    def main(self):
        RutaLicencia = "C:\\Users\\infra\\Proyectos\\monteSinai\\code\\InfraIntelisisUtilities\\ins\\Licencia.xml"

        if(os.path.isfile(RutaLicencia) == False):
            p.print_error("No se encontro el archivo de Licencia %s" % (RutaLicencia))

        Licencia = "NCO006"
        fs = "2018-06-28T10:28:43"
        fv = "2019-12-31T00:00:00"
        md5a = "Control"

        Total = 16;

        dd:=self.SL_Firmar(RutaIntelisisSL, RutaLicencia, "A632C200-7FF5-43C7-BE9E-F081BCB7173C");
        md5c = self.SL_CuerdaControlSLHQ(Licencia, dd, "A632C200-7FF5-43C7-BE9E-F081BCB7173C", Total, fs, fv);
        dc:=SL_MD5String(md5c);
        md5c:=UpperCase(SL_MD5Print(dc));

        NodoC:=NXML.Root.FindNode(Control);
        NodoC.Delete;

        NXML.XmlFormat:=xfReadable;
        XML:=NXML.WriteToString;
        dx:=SL_MD5String(XML);
        md5x:=UpperCase(SL_MD5Print(dx));

        md5t:=md5x+md5c;
        dt:=SL_MD5String(md5t);
        md5t:=UpperCase(SL_MD5Print(dt));

        p.print_info(md5t)

