'''
Created on Apr 25, 2017

@author: infra
'''

from pyInfraUtils.LogPrinter import p
from IIULauncher.IIULauncher import c
from IIULauncher.IIULauncher import ap
import IIUHandlers.BaseHandler

import os, sys, xlsxwriter, io, datetime
from xlrd import open_workbook
from types import NoneType
from __builtin__ import str
from _sqlite3 import Row

class presupuesto(IIUHandlers.BaseHandler.BaseHandler):
    
    accent_letters = [(unichr(193), 'A'), (unichr(201), 'E'), (unichr(205), 'I'), (unichr(211), 'O'), (unichr(218), 'U')]
    
    def readFile(self, filename):
        filepath = os.path.dirname(os.path.realpath(sys.argv[0]))+"/"+filename
        p.print_info("Leyendo: %s" % (filepath))
        if(os.path.isfile(filepath) == False):
            p.print_error("No se encontro el archivo "+filename)
            p.print_debug(filepath)
            return
           
        wb = open_workbook(filepath)
        sheet = wb.sheet_by_name("EGRESOS")
        number_of_rows = sheet.nrows

        i=1
        presupuesto = {}
        comite = ''
        subcomite = ''
        seccion = ''

        start_row = 5
        level_col = 0
        comite_col = 1
        conceptos_col = 19
        UEN_col = 20
        Ceco1_col = 21
        Ceco2_col = 22
        Ceco3_col = 23

        presupuesto_cols = {
            1: 3,
            2: 4,
            3: 5,
            4: 6,
            5: 7,
            6: 8,
            7: 9,
            8: 10,
            9: 11,
            10: 12,
            11: 13,
            12: 14
        }

        for row in range(start_row, number_of_rows):
            level = sheet.cell_value(row, level_col)
            
            if(level == 1):
                comite = sheet.cell_value(row, comite_col)
                continue
            elif (level == 2):
                subcomite = sheet.cell_value(row, comite_col)
                continue
            elif (level == 3):
                seccion = sheet.cell_value(row, comite_col)
            else:
                p.print_debug("saltando row %s porque su fila de nivel (%s) no es 1-3" % (row, level))
                continue
            
            p.print_debug('com (%s)/sub (%s)/sec (%s)' % (comite, subcomite, seccion))
            if(comite.strip() == '' or subcomite.strip() == '' or seccion.strip() == ''):
                p.print_debug("Saltando row %s pero un valor de com (%s)/sub (%s)/sec (%s) esta vacio" % (row, comite, subcomite, seccion))

            conceptos_lista = sheet.cell_value(row, conceptos_col)
            if str(conceptos_lista).strip() == "":
                p.print_debug("Saltando row %s porque no tiene lista de conceptos" % (row))
                continue
            conceptos = conceptos_lista.split(", ")
            p.print_debug(conceptos)
            
            UEN = sheet.cell_value(row, UEN_col)
            if(type(UEN) == float):
                UEN = int(UEN)
            UEN = str(UEN)
            
            Ceco1_lista = sheet.cell_value(row, Ceco1_col)
            if(type(Ceco1_lista) == float):
                Ceco1_lista = int(Ceco1_lista)
            Ceco1_lista = str(Ceco1_lista)
            Cecos1 = Ceco1_lista.split(", ")

            Ceco2_lista = sheet.cell_value(row, Ceco2_col)
            if(type(Ceco2_lista) == float):
                Ceco2_lista = int(Ceco2_lista)
            Cecos2 = str(Ceco2_lista)

            Ceco3_lista = sheet.cell_value(row, Ceco3_col)
            if(type(Ceco3_lista) == float):
                Ceco3_lista = int(Ceco3_lista)
            Cecos3 = str(Ceco3_lista)
            
            #padear la lista de Cecos para que siempre sean 4
            #for x in range(len(Cecos), 4):
            #    Cecos.append('')
            
            # Aqui lo convierto a string y con encoding para que al escribir en Excel no lo tenga que convertir.
            ppto_mensual = {
                1: sheet.cell_value(row, presupuesto_cols[1]),
                2: sheet.cell_value(row, presupuesto_cols[2]),
                3: sheet.cell_value(row, presupuesto_cols[3]),
                4: sheet.cell_value(row, presupuesto_cols[4]),
                5: sheet.cell_value(row, presupuesto_cols[5]),
                6: sheet.cell_value(row, presupuesto_cols[6]),
                7: sheet.cell_value(row, presupuesto_cols[7]),
                8: sheet.cell_value(row, presupuesto_cols[8]),
                9: sheet.cell_value(row, presupuesto_cols[9]),
                10: sheet.cell_value(row, presupuesto_cols[10]),
                11: sheet.cell_value(row, presupuesto_cols[11]),
                12: sheet.cell_value(row, presupuesto_cols[12]),
                }
            
            #comite = sheet.cell_value(row, 60)
            #subcomite = sheet.cell_value(row, 61)
            #seccion = sheet.cell_value(row, 1)
            
            #p.print_debug("%s->%s (U: %s, C: %s)" % (comite, conceptos_lista, UEN, Ceco_lista))
            #p.print_debug(ppto_mensual)

            presupuesto[i] = {
                'conceptos': [],
                'UEN': UEN,
                'Cecos1': Cecos1,
                'Cecos2': Cecos2,
                'Cecos3': Cecos3,
                'Comite': comite,
                'SubComite': subcomite,
                'Seccion': seccion,
                'mensual': ppto_mensual
            }
            for concepto in conceptos:
                if concepto == '':
                    continue

                presupuesto[i]['conceptos'].append(concepto)

            i+=1
            
        return presupuesto
        
    def writeToXLSX(self, presupuesto):
        if not presupuesto:
            p.print_error('Objeto presupuesto vacio')
            return
        
        if ap.arg('file').strip() == '':
            filename = 'output-ppto_new'
        else:
            filename = ap.arg('file').strip()
            
        workbook = xlsxwriter.Workbook('outs/%s.xlsx' % (filename), {'constant_memory': True})
        worksheet = workbook.add_worksheet()
        
        worksheet.set_column(0, 0, 50)
        worksheet.set_column(1, 1, 4)
        worksheet.set_column(2, 4, 6.8)
        worksheet.set_column(5, 16, 12)
        
        worksheet.write(0, 0, "Concepto")
        worksheet.write(0, 1, "UEN")
        worksheet.write(0, 2, "Ceco1")
        worksheet.write(0, 3, "Ceco2")
        worksheet.write(0, 4, "Ceco3")
        worksheet.write(0, 5, "Ceco4")
        worksheet.write(0, 6, "Enero")
        worksheet.write(0, 7, "Febrero")
        worksheet.write(0, 8, "Marzo")
        worksheet.write(0, 9, "Abril")
        worksheet.write(0, 10, "Mayo")
        worksheet.write(0, 11, "Junio")
        worksheet.write(0, 12, "Julio")
        worksheet.write(0, 13, "Agosto")
        worksheet.write(0, 14, "Septiembre")
        worksheet.write(0, 15, "Octubre")
        worksheet.write(0, 16, "Noviembre")
        worksheet.write(0, 17, "Diciembre")
        
        i=1
        for oneitem in presupuesto:
            if oneitem['concepto'] == '':
                continue
            
            worksheet.write(i, 0, oneitem['concepto'])
            worksheet.write(i, 1, oneitem['UEN'])
            worksheet.write(i, 2, oneitem['Ceco1'])
            worksheet.write(i, 3, oneitem['Ceco2'])
            worksheet.write(i, 4, oneitem['Ceco3'])
            worksheet.write(i, 5, oneitem['Ceco4'])
            worksheet.write(i, 6, oneitem['mensual'][1])
            worksheet.write(i, 7, oneitem['mensual'][2])
            worksheet.write(i, 8, oneitem['mensual'][3])
            worksheet.write(i, 9, oneitem['mensual'][4])
            worksheet.write(i, 10, oneitem['mensual'][5])
            worksheet.write(i, 11, oneitem['mensual'][6])
            worksheet.write(i, 12, oneitem['mensual'][7])
            worksheet.write(i, 13, oneitem['mensual'][8])
            worksheet.write(i, 14, oneitem['mensual'][9])
            worksheet.write(i, 15, oneitem['mensual'][10])
            worksheet.write(i, 16, oneitem['mensual'][11])
            worksheet.write(i, 17, oneitem['mensual'][12])
            
            i+=1
            
    def printToScreen(self, presupuesto):
        if not presupuesto:
            p.print_error('Objeto presupuesto vacio')
            return
        
        for index, oneitem in presupuesto.iteritems():
            p.print_info(oneitem)
            
        return
    
    def writeToFileObject(self, afile, line):
        if type(line) == str:
            for k, v, in self.accent_letters:
                line = line.replace(k, v)
        elif type(line) == unicode:
            for k, v, in self.accent_letters:
                line = line.replace(k, v)            

        afile.write(u'' + line + '\n')
    
    def writeSQLFile(self, presupuesto):
        if type(ap.arg('file')) == NoneType or ap.arg('file').strip() == '':
            filename = 'output-ppto_new'
        else:
            filename = ap.arg('file').strip()

        comites = {}
        for keyitem, pptoitem in presupuesto.iteritems():            
            comite = pptoitem['Comite']
            subcomite = pptoitem['SubComite']
            seccion = pptoitem['Seccion']

            if comite not in comites:
                comites[comite] = {}
            
            if subcomite not in comites[comite]:
                comites[comite][subcomite] = {}

            if seccion not in comites[comite][subcomite]:
                comites[comite][subcomite][seccion] = {
                        'conceptos': pptoitem['conceptos'],
                        'UEN': pptoitem['UEN'],
                        'Cecos1': pptoitem['Cecos1'],
                        'Cecos2': pptoitem['Cecos2'],
                        'Cecos3': pptoitem['Cecos3'],
                        'mensual': pptoitem['mensual'],
                    }
            else:
                comites[comite][subcomite][seccion]['conceptos'].append(keyconcepto)

        sqlFile = io.open('outs/'+filename+'.sql', 'w+', newline='\r\n')
        sqlFile.close()
        sqlFile = io.open('outs/'+filename+'.sql', 'a+', newline='\r\n')
        self.writeToFileObject(sqlFile, '''BEGIN TRANSACTION
TRUNCATE TABLE ConceptoComite
TRUNCATE TABLE Comite
TRUNCATE TABLE ComiteSubComite
TRUNCATE TABLE ComiteSeccion
TRUNCATE TABLE ComiteSeccionPresupuesto
GO''')
        
        #p.print_info(comites['PRESIDENCIA'])

        for comite,comitedata in comites.iteritems():
            comite = comite.strip()
            self.writeToFileObject(sqlFile,'/********************************* Comite: %s *********************************/' % (comite))
            #spCreateComite
            for subcomite,subcomitedata in comitedata.iteritems():
                subcomite = subcomite.strip()
                self.writeToFileObject(sqlFile,'/******************* SubComite: %s *******************/' % (subcomite))
                #spCreateSubComite
                for seccion,secciondata in subcomitedata.iteritems():
                    seccion = seccion.strip()
                    self.writeToFileObject(sqlFile,'/*********** Seccion: %s ***********/' % (seccion))
                    self.writeToFileObject(sqlFile, 'DECLARE @IDSeccion int')
                    self.writeToFileObject(sqlFile, 'EXEC spSysMS_CrearComiteSeccion \'%s\', \'%s\', \'%s\', @IDSeccion OUTPUT' % (comite, subcomite, seccion))
                    for concepto in secciondata['conceptos']:
                        concepto = concepto.strip()
                        p.print_debug("Relacionando C: %s S: %s X: %s t: %s" % (comite, subcomite, seccion, concepto))

                        for ceco1 in secciondata['Cecos1']:
                            self.writeToFileObject(sqlFile, 'EXEC spSysMS_RelacionarConceptoComiteSeccion \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\'' % (concepto, ceco1, secciondata['Cecos2'], secciondata['Cecos3'], secciondata['UEN'], comite, subcomite, seccion))

                    p.print_debug(comite+'->'+subcomite+'->'+seccion+' '+str(secciondata['conceptos']))
                    
                    for month in range(1, 13):
                        if str(secciondata['mensual'][month]).strip() == "" or float(secciondata['mensual'][month]) == 0.0:
                            continue
                        
                        self.writeToFileObject(sqlFile, 'EXEC spSysMS_ComiteSeccionAsignarPresupuesto @IDSeccion, \'MS\', %d, %d, %f, \'Pesos\'' % (2017, month, float(secciondata['mensual'][month])))
                        continue 

                        sqlFile.write(u''+'INSERT INTO Gasto (Empresa, Mov, FechaEmision, UltimoCambio, Acreedor, Moneda, TipoCambio, Usuario, Estatus, Importe, UEN) ')
                        movdate = '2017/%s/1' % month
                        curdate = datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
                        sqlFile.write(u''+'VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %f, \'%s\', \'%s\', %s, \'%s\')' % ('MS', 'Presup. Acumulado', movdate, curdate, 'PRESUP', 'Pesos', 1.0, 'NESKENAZI', 'SINAFECTAR', float(secciondata['mensual'][month]), secciondata['UEN']))
                        
                        self.writeToFileObject(sqlFile, 'SET @ID = SCOPE_IDENTITY()')
                        renglon = 2048
                        
                        for concepto in secciondata['conceptos']:
                            concepto = concepto.strip()
                            if secciondata['Ceco1'].strip() == '':
                                p.print_error("Co:%s Su:%s Se:%s - No tiene Ceco1" % (comite, subcomite, seccion))
                                continue
                            
                            sqlFile.write(u''+ 'INSERT INTO GastoD (ID, Renglon, Concepto, Fecha, Cantidad, Precio, Importe, ContUso)'+ '\n')
                            sqlFile.write(u''+ 'VALUES (@ID, %i, \'%s\', \'%s\', 1, 0, 0, \'%s\')' % (renglon, concepto, curdate, secciondata['Ceco1'].strip()))
                            
                            if secciondata['Ceco2'].strip() != '':
                                renglon = renglon + 2048
                                sqlFile.write(u''+ ',(@ID, %i, \'%s\', \'%s\', 1, 0, 0, \'%s\')' % (renglon, concepto, curdate, secciondata['Ceco2'].strip()))
                                
                            if secciondata['Ceco3'].strip() != '':
                                renglon = renglon + 2048
                                sqlFile.write(u''+ ',(@ID, %i, \'%s\', \'%s\', 1, 0, 0, \'%s\')' % (renglon, concepto, curdate, secciondata['Ceco3'].strip()))
                                
                            if secciondata['Ceco4'].strip() != '':
                                renglon = renglon + 2048
                                sqlFile.write(u''+ ',(@ID, %i, \'%s\', \'%s\', 1, 0, 0, \'%s\')' % (renglon, concepto, curdate, secciondata['Ceco4'].strip()))
                            
                            renglon = renglon + 2048
                            
                            #crear relacion comite/sub/secc con concepto y cecos
    
    
                        self.writeToFileObject(sqlFile, 'EXEC spAfectar \'GAS\', @ID, \'AFECTAR\', \'Todo\', @Usuario = \'NESKENAZI\', @EnSilencio = 1, @Ok = @Ok OUTPUT, @OkRef = @OkRef OUTPUT')
                        self.writeToFileObject(sqlFile, 'IF ISNULL(@Ok, \'\') != \'\'')
                        self.writeToFileObject(sqlFile, 'BEGIN')
                        self.writeToFileObject(sqlFile, 'SELECT \'ERROR: \'+CONVERT(varchar, @Ok)+\' \'+@OkRef')
                        self.writeToFileObject(sqlFile, '\t' + 'ROLLBACK TRANSACTION')
                        self.writeToFileObject(sqlFile, '\t' + 'raiserror(\'Error en transaccion\', 19, -1)')
                        self.writeToFileObject(sqlFile, 'END')
                    self.writeToFileObject(sqlFile, 'GO')
                        
        self.writeToFileObject(sqlFile, 'ROLLBACK')
                    
    def main(self):
        if ap.arg('origin') == '' or ap.arg('origin') == None:
            p.print_error("No se definio el origen", True)

        if ap.arg('display') == None or ap.arg('display') == '':
            displayType = 'screen'
        else:
            displayType = ap.arg('display')

        infile = ap.arg('origin').strip()
        if infile != '':
            presupuesto = self.readFile(infile)
        else:
            p.print_error("No se definio el archivo a leer")

        if displayType == 'screen':
            self.printToScreen(presupuesto)
        if displayType == 'xls':
            self.writeToXLSX(presupuesto)
        if displayType == 'sql':
            self.writeSQLFile(presupuesto)