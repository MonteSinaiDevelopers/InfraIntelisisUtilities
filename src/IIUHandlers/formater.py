'''
Created on Apr 25, 2017

@author: infra
'''

from pyInfraUtils.FilesCommon import ScanDirectory
from pyInfraUtils.FilesCommon import random_char 
from pyInfraUtils.LogPrinter import p
from IIULauncher.IIULauncher import c
import IIUHandlers.BaseHandler

import os, cchardet, io

class formater(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''
    def format_files(self, dir):
        dirpath = c.cfg("General", "BaseDir")+"/"+dir
        if(os.path.isdir(dirpath) == False):
            return False
        
        files = ScanDirectory(dirpath)
        
        if(not files):
            return False
        
        for filename, onefile in files.iteritems():
            if(os.path.isfile(dirpath+"/"+filename) == False):
                continue
            
            with open(dirpath+"/"+filename) as f:
                msg = f.read()
                result = cchardet.detect(msg)
                if('encoding' in result and result['encoding'] == "UTF-8"):
                    sourcepath = dirpath+"/"+filename 
                    source = io.open(sourcepath, )
                    targetpath = os.getcwd()+"/temp/"+random_char(5)
                    target = io.open(targetpath, "w", newline='\r\n')
                    
                    target.write(unicode(source.read(), "UTF-8").encode("ISO-8859-1"))
                    os.rename(targetpath, sourcepath)
                    p.print_success("Convertido: "+filename)
        
    def main(self):
        # Jalar esto de la configuracion, 
        if(self.arg_list['origin'] == 'trunk'):
            self.format_files(c.cfg("General", "project-dir"))