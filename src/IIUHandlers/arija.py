'''
Created on Jan 1, 2018

@author: infra
'''

from pyInfraUtils.LogPrinter import p
import IIUHandlers.BaseHandler

import os, io
from xlrd import open_workbook
from types import NoneType

class arija(IIUHandlers.BaseHandler.BaseHandler):
    '''
    classdocs
    '''
    accent_letters = [(unichr(193), 'A'), (unichr(201), 'E'), (unichr(205), 'I'), (unichr(211), 'O'), (unichr(218), 'U')]
    workbook = None
    column_numbers = {
        "id_socio": 0,
        "importe": 13
    }

    def printColumnNames(self, filepath):
        filename = os.path.basename(filepath)
        p.print_info("Leyendo: %s" % (filepath))
        if(os.path.isfile(filepath) == False):
            p.print_error("No se encontro el archivo "+filename)
            p.print_debug(filepath)
            return
           
        self.workbook = open_workbook(filepath)
        sheet = self.workbook.sheet_by_name("OK")
        number_of_cols = sheet.ncols

        for rowno in range(0, number_of_cols-1):
            p.print_info(str(rowno)+") "+sheet.cell_value(0, rowno))
            
        
        id_socio_column = raw_input("Seleccione el numero de columna donde se incluye el ID_Socio: ")
        if(id_socio_column != ""):
            self.column_numbers["id_socio"] = int(id_socio_column)
        
        importe_column = raw_input("Seleccione el numero de columna donde se incluye el importe de la Arija 2018: ")
        if(importe_column != ""):
            self.column_numbers["importe"] = int(importe_column)
        
        return
    
    def writeToFileObject(self, afile, line):
        if type(line) == str:
            for k, v, in self.accent_letters:
                line = line.replace(k, v)
        elif type(line) == unicode:
            for k, v, in self.accent_letters:
                line = line.replace(k, v)            

        afile.write(u'' + line + '\n')

    def process(self):
        sheet = self.workbook.sheet_by_name("OK")
        numer_of_rows = sheet.nrows
        
        if 'file' not in self.arg_list or type(self.arg_list['file']) == NoneType or self.arg_list['file'].strip() == '':
            filename = 'output-arija'
        else:
            filename = self.arg_list['file']
        
        sqlFile = io.open('outs/'+filename+'.sql', 'w+', newline='\r\n')
        sqlFile.close()
        sqlFile = io.open('outs/'+filename+'.sql', 'a+', newline='\r\n')
        
        self.writeToFileObject(sqlFile, 'DECLARE @ID int, @Agente varchar(10)')
        self.writeToFileObject(sqlFile, 'BEGIN TRANSACTION')

        for row in range(1, numer_of_rows):
            socio_raw = sheet.cell_value(row, self.column_numbers["id_socio"])
            importe_raw = sheet.cell_value(row, self.column_numbers["importe"])
            if(str(importe_raw) != ''and
               importe_raw != 0 and
               str(socio_raw) != ''):
                id_socio = int(socio_raw)
                importe = float(importe_raw)
                p.print_debug("Socio: %d, Importe: %f" % (id_socio, importe))
                
                if(importe > 0 or id_socio != ''):
                    #         INSERT Venta (Empresa, Mov, MovID, FechaEmision, Concepto, Moneda, TipoCambio, UEN, FechaRequerida, Usuario, Referencia, Estatus, Cliente, EnviarA, RenglonID, Almacen, Vencimiento, Importe, Impuestos, Ejercicio, Periodo, FechaRegistro, FechaConclusion, Sucursal, Condicion, Agente, MONSIN_Concepto, MONSIN_SubConcepto, MONSIN_Templo)
                    #     VALUES(@Empresa, 'Arija', NULL, @Fecha, 'ARIJOT', @Moneda, @TipoCambio, 7, @Fecha, @Usuario, CONVERT(CHAR(4), @Ejercicio), 'SINAFECTAR', @Cliente, @EnviarA, 1, @Almacen, @FechaVence, @Importe, 0, @Ejercicio, @Periodo, @Fecha, @Fecha, @Sucursal, '(Fecha)', @Agente, '01 ARIJOT', '01  ARIJA', '99 MONTE SINAI') 
                    self.writeToFileObject(sqlFile, "SELECT @Agente = ISNULL(Agente, '%s') FROM Cte WHERE Cliente = '%s'" % ("09", str(id_socio)))
                    
                    self.writeToFileObject(sqlFile, "INSERT Venta (Empresa, Mov, MovID, FechaEmision, Concepto, Moneda, TipoCambio, UEN, FechaRequerida, Usuario, Referencia, Estatus, Cliente, EnviarA, RenglonID, Almacen, Vencimiento, Importe, Impuestos, Ejercicio, Periodo, FechaRegistro, FechaConclusion, Sucursal, Condicion, Agente, MONSIN_Concepto, MONSIN_SubConcepto, MONSIN_Templo)")
                    self.writeToFileObject(sqlFile, "VALUES('%s', '%s', NULL, '%s', '%s', '%s', 1, 7, '%s', '%s','%s', '%s', '%s', NULL, 1, '%s', '%s', %f, 0, '%s', 1, '%s', '%s', 0, '%s', @Agente, '%s', '%s', '%s')" % ("MS", "Arija", "1/1/2018", "ARIJOT", "Pesos", "1/1/2018", "NESKENAZI", "2018", "SINAFECTAR", str(id_socio), "99", "4/15/2018", importe, "2018", "1/1/2018", "1/1/2018", "(Fecha)", "01 ARIJOT", "01  ARIJA", "99 MONTE SINAI"))
                    self.writeToFileObject(sqlFile, "SET @ID = SCOPE_IDENTITY()")
                    self.writeToFileObject(sqlFile, "INSERT INTO VentaD (ID,  Renglon,  RenglonSub, RenglonID,  RenglonTipo,  Impuesto1,  Impuesto2, Impuesto3, Almacen,  Articulo,  SubCuenta,  Cantidad,  Unidad,  CantidadInventario, Precio,   Sucursal)  ")
                    self.writeToFileObject(sqlFile, "VALUES (@ID, 2048.0, 0, 1, '%s', 0, NULL,      NULL,      '%s', '%s', NULL,       1, '%s', 1,          %f, 0)" % ("N", "99", "01", "N/A", importe))
                    
                    self.writeToFileObject(sqlFile, '')
            
        #   self.writeToFileObject(sqlFile, '')
        self.writeToFileObject(sqlFile, 'ROLLBACK')

    def main(self):
        filepath = self.arg_list['origin'].strip()
        self.printColumnNames(filepath)
        
        p.print_debug(self.column_numbers)
        self.process()
        
        