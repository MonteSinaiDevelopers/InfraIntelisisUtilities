'''
Created on Mar 30, 2017

@author: infra
'''

import importlib, imp, signal, argparse, sys
from pyInfraUtils.Config import pIUConfig
c = pIUConfig()

import pyInfraUtils.ArgParser
ap = pyInfraUtils.ArgParser.IIUArgParser()

ap.setDescription("InfraIntelisisUtilities")
ap.parseArgs()

#Tengo que hacer este import aqui porque depende de que los args hayan sido parseados, e importar el self.arg_list
from pyInfraUtils.LogPrinter import p

class IIULauncher():
  
    def __init__(self):
        action = ap.arg("action")

        #acciones disponibles, llave es el string de la accion pasado en el segundo argumento y el valor es el objeto llamado
        actions = {
            "status-one": "status",
            "format": "formater",
            "presup": "presupuesto",
            "ppto": "presupuesto",
        }
        
        def signal_handler(signal, frame):
            p.print_info("CTRL+C detectado")
            import sys 
            sys.exit(0)
        
        signal.signal(signal.SIGINT, signal_handler)

        if(action != None):
            if(action in actions):
                module_name = "IIUHandlers."+actions[action]
                module_imported = importlib.import_module(module_name)
                
                constructor = getattr(module_imported, actions[action])
                instance = constructor()
                instance.main()
            else:
                module_name = "IIUHandlers."+action
                try:
                    module_info = imp.find_module('IIUHandlers')
                    IIU_module = imp.load_module('IIUHandlers', *module_info)
                    imp.find_module(action, IIU_module.__path__) # __path__ is already a list
                except ImportError:
                    p.print_error("No se encontro el modulo %s" % (action))
                    sys.exit()

                module_imported = importlib.import_module(module_name)
                
                constructor = getattr(module_imported, action)
                instance = constructor()
                instance.main()